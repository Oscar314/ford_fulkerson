import time
from Node import Node, Edge
import Pygame
import FF

RED = (255,0,0)
GREEN = (0,255,0)
BLUE = (0,0,255)
sensitivity = 20
screenSize = 1000

class Handler:
    def __init__(self):
        self.info = ["Press 'N' to create nodes", "'S' to select source/sink", 
                    "'L' to create edges", "'E' to edit edges", "'R' to run"]
        self.lookUp = {"e": "Edit edges", "l": "Place edges", "n": "Place nodes", "s": "Source/Sink", "r": "Run"}
        self.stage = self.lookUp["n"]
        self.selectedNode = None
        self.selectedEdge = None
        self.errorMsg = ""
        self.errorTime = 0
        self.running = True
        self.endings = set()
        self.edges = set()
        self.nodes = set()
    
    def setNodeColor(self):
        """ Sets the color of every node. """
        for node in self.nodes:
            if node in self.endings:
                node.color = BLUE
            elif self.selectedNode == node:
                node.color = GREEN
            else:
                node.color = RED

    def generateNode(self, clickPosition):
        """ Instantiates a node and puts it in self.nodes """
        node = Node(RED, clickPosition)
        self.nodes.add(node)

    def generateEdge(self, node):
        """ Instantiates an edge if two nodes have been selected. """
        if self.selectedNode is None:   # No selected node => selected node = current
            self.selectedNode = node
        
        elif self.selectedNode == node:   # two clicks on same node => unselect
            self.selectedNode = None
            
        else:
            edge = Edge(self.selectedNode, node)
            self.edges.add(edge)
            self.selectedNode = node

    def findClose(self, clickPosition):                                               ##SHould pick the closest!                  swjidwjidjiw
        """ If existing, finds and returns the first found node 
        and edge closer than sensitivity pixels from clickposition. """
        close = {"n": None, "e": None}
        distanceBetween = lambda p1, p2: sum(map(lambda a, b: (a-b)**2, p1, p2))**0.5
        for edge in self.edges:
            if distanceBetween(edge.middlePoint, clickPosition) < sensitivity:
                close["e"] = edge
                continue
        for node in self.nodes:
            if distanceBetween(node.pos, clickPosition) < sensitivity:
                close["n"] = node
                continue

        return close
        
    def click(self, clickPosition):
        """ Depending on the current stage determines what to do if user clicked the screen. """
        close = self.findClose(clickPosition)

        if close["e"] is not None and self.stage == self.lookUp["e"]:    # Clicked on a edge
            self.selectedEdge = close["e"]

        elif close["n"] is not None and self.stage == self.lookUp["l"]:  # Clicked on a node
            self.generateEdge(close["n"])

        elif close["n"] is not None and self.stage == self.lookUp["s"]:  # Edit source/sink
            self.setSourceSink(close["n"])

        elif close["n"] is None and self.stage == self.lookUp["n"]:
            self.generateNode(clickPosition)


    def setSourceSink(self, node):
        if node in self.endings:
            self.endings.remove(node)

        elif len(self.endings) > 1:
            self.errorMsg = "Endings full, unselect source/sink"
            self.errorTime = time.time()
        else:
            self.endings.add(node)


    def keyPressed(self, key):
        """ Determines what to do if a key is pressed. """
        self.stage = self.lookUp.get(key.unicode, self.stage)
        if self.stage == self.lookUp["r"]:
            self.runFF()
        elif self.selectedEdge is not None and self.stage == self.lookUp["e"]:
            self.selectedEdge.setWeight(key.unicode)

    def draw(self):
        """ Clears the screen. Draws text and every node/edge. """
        Pygame.clear()
        self.setNodeColor()
        for item in self.nodes | self.edges:
            item.draw()

        Pygame.drawText("Current mode: " + self.stage, (screenSize*3/4, 20))
        if self.stage == self.lookUp["r"]:
            Pygame.drawText("Press enter to continue", (screenSize*3/4, screenSize -20))

        Pygame.drawText(self.errorMsg, (screenSize/2, screenSize-20))
        Pygame.drawInfo(self.info)
        Pygame.update()

    def handleEvents(self):
        """ Checks every event. If interesting sends it to the correct method. """
        eventlst = Pygame.events()
        for event in eventlst:
            if isinstance(event, tuple):  # User clicked on screen
                self.click(event)
            elif not event:               # User exited the window
                self.running = False
            else:                         # User pressed a key
                self.keyPressed(event)

    def createGraph(self):
        """ Creates a graph as a dict. 
        Each node in the graph has a dict where every node it is 
        connected to is a key and the edge connecting them the value. 
          """

        graph = {}
        for edge in self.edges:
            u, v = edge.node1, edge.node2
            
            if u in graph:
                graph[u][v] = edge
            else:
                graph.update({u: {v: edge}})

            if v in graph:
                graph[v][u] = edge
            else:
                graph.update({v: {u: edge}})

        return graph

    def runFF(self):
        if len(self.endings) != 2:
            self.errorMsg = "Must select start and endNode"
            self.errorTime = time.time()
            Pygame.update()
            self.stage = self.lookUp["s"]
            return
        
        start, end = [i for i in self.endings]
        #self.edges.add(Edge(start, end))
        graph = self.createGraph()
        maximumFlow = FF.ford_fulkerson(graph, start, end, self)
        Pygame.drawText("Maximum flow is: " + str(maximumFlow) + ". Press enter to quit", (screenSize/2, screenSize-20))
        Pygame.update()
        self.waitForEnter()
        self.running = False


    def waitForEnter(self):
        """ Waits for the user to press enter. Uses polling. """
        while True:
            lst = Pygame.events()
            time.sleep(0.1)
            for l in lst:
                try:
                    if l.unicode == "\r":
                        return
                except Exception:
                    pass


    def run(self):
        """ Run-loop, handles all events and draws on the screen. """
        while self.running:
            Pygame.tick(30)
            if self.errorTime + 5 < time.time():
                self.errorMsg = ""
            self.draw()
            self.handleEvents()

handler = Handler()
handler.run()

