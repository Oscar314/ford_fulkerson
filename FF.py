from collections import deque

def BFS(graph, s, t, pred):
    visited = {}

    for v in graph.keys():
        visited[v] = False

    visited[s] = True
    q = deque([s])
    print(q)
    while q:
        try:
            v = q.popleft()
            for w in graph[v].keys():
                if not visited[w] and graph[v][w].residualCapacity > 0:
                    q.append(w)
                    visited[w] = True
                    pred[w] = v

                    if w == t:
                        return True
        except Exception:
            return False

    return False

def ford_fulkerson(graph, s, t, h):
    for u in graph:
        for v in graph[u]:
            e = graph[u][v]
            e.residualCapacity = int(e.weight)
    h.draw()
    h.waitForEnter()
    totalflow = 0
    pred = {}

    while BFS(graph, s, t, pred):
        amoutOfFlow = float("Inf")
        v = t
        while v != s:
            amoutOfFlow = min(amoutOfFlow, graph[pred[v]][v].residualCapacity)
            v = pred[v]

        v = t
        while v != s:
            u = pred[v]
            graph[u][v].residualCapacity -= amoutOfFlow
            v = u

        totalflow += amoutOfFlow
        h.draw()
        h.waitForEnter()

    return totalflow
