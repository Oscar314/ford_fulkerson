import math
import Pygame

class Node:
    def __init__(self, color, pos):
        self.color = color
        self.pos = pos

    def draw(self):
        Pygame.circle(self.color, self.pos)


class Edge:
    def __init__(self, node1, node2):
        self.residualCapacity = 0
        self.weight = 0
        self.node1 = node1
        self.node2 = node2
        self.position1 = node1.pos
        self.position2 = node2.pos
        self.middlePoint = tuple(map(lambda a, b: int((a+b)/2), self.position1, self.position2))
        self.lineEnding1, self.lineEnding2 = self.setPoints()

    def __eq__(self, other):
        """ A edge is equal another if it has the same nodes. """
        equal = self.node1 == other.node1 and self.node2 == other.node2
        switched = self.node1 == other.node2 and self.node2 == other.node1
        return equal or switched

    def __hash__(self):
        """ Has to be overwritten to make Set work correctly. """
        return hash(self.position1) + hash(self.position2)


    def setPoints(self):
        """ To draw the two lines in draw() four points are needed,
             the points of the nodes and point1 and point2."""  
        space = 40/2  # the space in the middle set to 40 pixels
        if self.position1[1] > self.position2[1]:        # Make sure position1 is the 'highest'
            self.position1, self.position2 = self.position2, self.position1

        elif self.position1[1] == self.position2[1] and self.position1[0] > self.position2[0]:
            self.position1, self.position2 = self.position2, self.position1

        if self.position1[1] == self.position2[1]:
            alpha = math.pi/2
        else:
            alpha = math.atan((self.position1[0]-self.position2[0])/(self.position1[1]-self.position2[1]))

        distanceToMiddle = sum(map(lambda a, b: (a-b)**2, self.position1, self.position2))**0.5/2
        delta = (math.sin(alpha), math.cos(alpha))
        point1 = tuple(map(lambda upperNode, d: upperNode + d*(distanceToMiddle - space), self.position1, delta))
        point2 = tuple(map(lambda upperNode, d: upperNode + d*(distanceToMiddle + space), self.position1, delta))

        return point1, point2
    
    def setWeight(self, unicode):
        """ Given a keypress sets the weight of the node. """
        if unicode == "\x08":    # BACKSPACE
            self.weight = int(self.weight/10)
        else:
            try:
                self.weight = self.weight*10 + int(unicode)  #Adds the pressed number to the "back" of the current weight
            except ValueError:
                print("Not an int")

    def draw(self):
        """ Draws a line between the edges nodes. 
        In the middle the used capacity and availible capaicity is written. """
        usedCapacity = self.weight - self.residualCapacity
        Pygame.line(self.position1, self.lineEnding1)
        Pygame.line(self.position2, self.lineEnding2)
        Pygame.drawText(str(usedCapacity) + "/" + str(self.weight), self.middlePoint)