import pygame

pygame.init()
screen = pygame.display.set_mode((1000, 1000))
clock = pygame.time.Clock()
font = pygame.font.Font('freesansbold.ttf', 25) 


def line(pos1, pos2):
    pygame.draw.line(screen, (0,0,0), pos1, pos2, 5)

def drawInfo(txtLst):
    for i, text in enumerate(txtLst):
        text = font.render(text, True, (0,0,0)) 
        textRect = text.get_rect()  
        textRect.topleft = (10,(i)*30)
        screen.blit(text, textRect) 

def drawText(text, center):
    text = font.render(text, True, (0,0,0)) 
    textRect = text.get_rect()  
    textRect.center = center
    screen.blit(text, textRect) 

def drawRect(color, pos):
    pygame.draw.rect(screen, color, pos)

def circle(color, pos):
    pygame.draw.circle(screen, color, pos, 10)

def clear():
    screen.fill((255, 255, 255))

def update():
    pygame.display.update()

def tick(time):
    clock.tick(time)

def events():
    lst = []
    for event in pygame.event.get():
        if event.type == pygame.MOUSEBUTTONDOWN:
            lst.append(pygame.mouse.get_pos())
        elif event.type == pygame.KEYDOWN:
            lst.append(event)
        elif event.type == pygame.QUIT:
            lst.append(False)    

    return lst

